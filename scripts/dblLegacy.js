const Pokemon = require('../json/pokemon');
const addTMCombinations = require('../lib/addTMCombinations');

const pokes = Pokemon.map((pokemon) => ({
  name: pokemon.name,
  moves: addTMCombinations(pokemon)
    .filter((move) => move.legacy === 3)
    .map((move) => `${move.A} & ${move.B}`),
})).filter((x) => x.moves.length > 0);

console.log(pokes);
