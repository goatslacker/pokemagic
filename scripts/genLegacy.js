/* eslint-disable import/no-extraneous-dependencies */
const axios = require('axios');
const babylon = require('babylon');
const cheerio = require('cheerio');
const fs = require('fs');
const generate = require('babel-generator').default;

const findPokemon = require('../lib/findPokemon');

const URL = 'https://p337.info/pokemongo/pages/legacy-moves/';

axios
  .get(URL)
  .then(
    (res) => {
      const $ = cheerio.load(res.data);
      const code = $('script')[3].children[0].data;
      const expr = babylon.parse(code);

      const legacy = JSON.parse(
        generate(expr.program.body[0].declarations[0].init).code
      );

      const output = {};

      legacy.response.forEach((info) => {
        const poke = findPokemon(info.name);
        if (!output[poke.id]) output[poke.id] = [];

        if (info.qlost) {
          info.qlost
            .split(',')
            .filter(Boolean)
            .forEach((move) => {
              output[poke.id].push(
                `${move
                  .trim()
                  .replace(/ /g, '_')
                  .toUpperCase()}_FAST`
              );
            });
        }

        if (info.clost) {
          info.clost
            .split(',')
            .filter(Boolean)
            .forEach((move) => {
              output[poke.id].push(
                move
                  .trim()
                  .replace(/ /g, '_')
                  .toUpperCase()
              );
            });
        }
      });

      fs.writeFileSync(
        './json/legacy.json',
        JSON.stringify(output, null, 2),
        'utf-8'
      );
    },
    (err) => console.error(err)
  )
  .catch((err) => console.error(err));
