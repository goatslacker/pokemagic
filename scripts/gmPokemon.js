const GameMaster = require('./GameMaster');

const prettyWrite = require('./prettyWrite');
const done = require('./done');

function type(t, pt) {
  if (t === pt) return null;

  return t ? t.replace('POKEMON_TYPE_', '') : null;
}

function dedupe(name, moves) {
  if (name !== 'MEW') return moves;
  return Array.from(new Set(moves));
}

function toID(str, name) {
  return str.replace(`_POKEMON_${name}`, '');
}

function dex(str) {
  return Number(str.slice(1, 5).replace(/^0\+/, ''));
}

const dupeForms = new Set([
  'CHERRIM', // CHERRIM_OVERCAST
  'GASTRODON', // GASTRODON_WEST
  'GIRATINA', // GIRATINA_ALTERED
  'SHAYMIN', // SHAYMIN_LAND
  'SHELLOS', // SHELLOS_WEST
]);

function removeDupeNormal(name, form) {
  if (dupeForms.has(name) && !form) {
    return false;
  }

  return form !== `${name}_NORMAL`;
}

GameMaster.then((data) => {
  const Pokemon = data.itemTemplates
    .filter((x) => x.hasOwnProperty('pokemonSettings'))
    .map(({ templateId, pokemonSettings }) => ({
      id: toID(templateId, pokemonSettings.pokemonId),
      dex: dex(templateId),
      name: pokemonSettings.pokemonId,
      form: pokemonSettings.form,
      type1: type(pokemonSettings.type),
      type2: type(pokemonSettings.type2, pokemonSettings.type),
      moves: {
        quick: dedupe(pokemonSettings.pokemonId, pokemonSettings.quickMoves),
        charge: dedupe(
          pokemonSettings.pokemonId,
          pokemonSettings.cinematicMoves
        ),
      },
      stats: {
        stamina: pokemonSettings.stats.baseStamina,
        attack: pokemonSettings.stats.baseAttack,
        defense: pokemonSettings.stats.baseDefense,
      },
      family: pokemonSettings.familyId,
      parentPokemonId: pokemonSettings.parentPokemonId,
      kmBuddyDistance: pokemonSettings.kmBuddyDistance,
      evolutionBranch: pokemonSettings.evolutionBranch,
      captureRate: pokemonSettings.encounter.baseCaptureRate,
      fleeRate: pokemonSettings.encounter.baseFleeRate,
      height: pokemonSettings.pokedexHeightM,
      weight: pokemonSettings.pokedexWeightKg,
    }))
    .filter(({ name, form }) => removeDupeNormal(name, form));

  prettyWrite('./json/pokemon.json', Pokemon);
  done('Pokemon');
});
