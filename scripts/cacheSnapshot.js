const fs = require('fs');

const genSnapshot = require('../lib/genSnapshot');

const snapshot = genSnapshot();

fs.writeFileSync(
  './test/snapshot.json',
  JSON.stringify(snapshot.battleSnapshot, null, 2)
);

fs.writeFileSync(
  './test/dmgOutputSnapshot.json',
  JSON.stringify(snapshot.dmgOutputSnapshot, null, 2)
);
