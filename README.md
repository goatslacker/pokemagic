# pokemagic

## Usage

```sh
npm install pokemagic
```

## API

### Pokedex

Example

```js
const dex = require('pokemagic/dex');

dex.findPokemon('jolteon'); // Pokemon
dex.findMove('struggle'); // Move
dex.getAllPokemon(); // [Pokemon]
dex.getMoves(); // [Moves]
```

### Battle Simulator

Example

```js
const simulateBattle = require('pokemagic/simulateBattle');

const attackers = [
  {
    iv: 0xfff,
    lvl: 40,
    name: 'machamp',
    move1: 'counter',
    move2: 'dynamic punch',
  },
];

const defenders = [
  {
    iv: 0xfff,
    lvl: 40,
    name: 'tyranitar',
    move1: 'bite',
    move2: 'crunch',
  },
];

const options = {
  raid: null,
  weather: 'EXTREME',
};

const stats = simulateBattle(attackers, defenders, options);

stats.winner === 'atk'; // true
```

Attacker/Defender Objects

```js
{
  iv: 'number', // hexadecimal
  lvl: 'number', // 1-40
  move1: 'string', // a valid Move name (see json/moves.json)
  move2: 'string',
  pokemon: { // a valid Pokemon object (see json/pokemon.json)
    id: 'number',
    name: 'string',
    type1: 'string',
    type2: 'string',
    stats: { stamina: 'number', attack: 'number', defense: 'number' },
  },
}
```

Request Options

```js
{
  atkDodgeStrategy: 'charge', // 'all' | 'charge' | null
  raid: {
    cp: 'number',
    hp: 'number',
    cpm: 'number',
  },
  raidTier: 'number',
  weather: 'string', // EXTREME, CLEAR, FOGGY, SUNNY (see lib/weather.js)
};
```

Response

```js
const Pokemon = {
  id: 'number',
  name: 'string',
  iv: 'string',
  moves: ['string', 'string'],
  cp: 'number',
  hp: 'number',
  dmgDealt: 'number',
  dmgTaken: 'number',
};

const Response = {
  log: [
    {
      // A log of all the moves that took place
      p: 'string', // Pokemon
      m: 'string', // Move
      dmg: 'number', // Damage
      ms: 'number', // Time
    },
  ],
  state: {
    atk: [Pokemon],
    def: [Pokemon],
  },
  timeElapsed: 'number',
  timeRemaining: 'number',
  timedOut: 'boolean',
  winner: 'string', // atk | def
};
```

### Breakpoint & Bulkpoint

Example

```js
const breakpoint = require('pokemagic/breakpoint');

// Calculate the break/bulk points for a Raikou fighting a Kyogre
const point = breakpoint('raikou', 'kyogre');
```

Response

```js
{
  atk: [{ // breakpoints
    move: 'string',
    table: [{
      dmg: 'number',
      cp: 'number',
      lvl: 'number',
      pct: 'string', // percentage increase over previous point
    }],
  }],
  def: [{ // bulkpoints
    move: 'string',
    table: [{
      dmg: 'number',
      cp: 'number',
      lvl: 'number',
      pct: 'string',
    }],
  }],
```

### Attacker/Defender Profile

Response

```js
{
  pokemon: Pokemon,
  raidInfo: {
    cp: "number",
    hp: "number",
    cpm: "number"
  },
  data: [{
    // Each move will have its own counters list
    quick: "string",
    charge: "string",
    // Each list belongs to a Pokemon, in case there are multiple movesets
    // that are viable counters
    results: [{
      name: "string",
      stats: [{
        dps: 'number',
        legacy: 'number', // 0=not legacy,1=quick legacy,2=charge legacy,3=double legacy,4=community day
        moves: ['string', 'string'], // Quick & Charge move
        name: 'string', // Pokemon's name
        score: 'number', // Internally used to rank best counters
        tdo: 'number',
      }],
    }],
  }],
  types: {
    immune: ['string'],
    notEffective: ['string'],
    superEffective: ['string'],
  },
}
```

### Type Rankings

Example

```js
const typeRankings = require('pokemagic/typeRankings');

const rank = typeRankings('electric');
```

Response

```js
[
  {
    name: 'string',
    moves: ['string', 'string'],
    dps: 'number', // Damage per second
    score: 'number', // Internally used to sort
    tdo: 'number', // Total time
    wins: 'number', // Number of battles won
    count: 'number', // Number of battles fought
  },
];
```

### IV Calculator

Example

```js
const calculateIV = require('pokemagic/calculateIV');

const matches = calculateIV(
  findPokemon('MEWTWO'), // Pokemon
  3982, // CP
  164, // HP
  40 // Level
);
```

Response

Possible IV values are returned.
If the array is empty then no IVs were matched.

```js
[{ atk: 'number', def: 'number', sta: 'number' }];
```
