const test = require('ava');

const attackerProfile = require('../lib/attackerProfile');
const findPokemon = require('../lib/findPokemon');

test('returns rampardos for heracross', (t) => {
  const profile = attackerProfile({ pokemon: findPokemon('heracross') });
  t.is(profile.data[1].results[0].id, 'V0409');
});

test('returns rampardos for sc/bulldoze mew', (t) => {
  const profile = attackerProfile({
    pokemon: findPokemon('mew'),
    quickMoveName: 'shadow claw',
    chargeMoveName: 'bulldoze',
  });
  t.is(profile.data[0].results[0].id, 'V0409');
});
