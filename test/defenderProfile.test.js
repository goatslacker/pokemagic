const test = require('ava');

const defenderProfile = require('../lib/defenderProfile');
const findPokemon = require('../lib/findPokemon');

test('returns raikou for kyogre', (t) => {
  const profile = defenderProfile({
    pokemon: findPokemon('kyogre'),
    raid: true,
    weather: 'EXTREME',
  });

  const quick = 'WATERFALL_FAST';
  const charge = 'THUNDER';

  const dataForWT = profile.data.find(
    (res) => res.quick === quick && res.charge === charge
  );

  t.is(!!dataForWT, true);

  const raikouStats = dataForWT.results.find((res) => res.name === 'RAIKOU');

  t.is(raikouStats.stats[0].moves[0], 'THUNDER_SHOCK_FAST');
  t.is(raikouStats.stats[0].moves[1], 'WILD_CHARGE');
});

test('blissey', (t) => {
  const profile = defenderProfile({
    chargeMoveName: 'dazzling gleam',
    numPokemon: 1,
    pokemon: findPokemon('bliss'),
    quickMoveName: 'ZEN_HEADBUTT_FAST',
    scoring: 'dps',
  });
  t.is(profile.data[0].results[0].name, 'MACHAMP');
  t.is(profile.data[0].results[0].stats[0].moves[0], 'COUNTER_FAST');
  t.is(profile.data[0].results[0].stats[0].moves[1], 'DYNAMIC_PUNCH');
});
