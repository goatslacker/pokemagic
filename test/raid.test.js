const test = require('ava');

const raid = require('../lib/raid');
const findPokemon = require('../lib/findPokemon');

test('custom tier case', (t) => {
  const hitmonlee = findPokemon('hitmonlee');

  const obj = raid(hitmonlee, 3);

  // Testing the custom tier case
  t.is(obj.cp, 18326);
});

test('cached pokemon', (t) => {
  const tyranitar = findPokemon('tyranitar');

  const obj = raid(tyranitar);

  // Testing the cached case
  t.is(obj.cp, 34707);
});

test('is a tier 3 is tier is not specified', (t) => {
  const golem = findPokemon('golem');

  const obj = raid(golem);

  // It's treated like a tier-3 pokemon if tier isn't specified
  t.is(obj.cp, 18065);
});

test('custom tiers work for cached pokemon', (t) => {
  const trex = findPokemon('tyran');
  const obj = raid(trex, 2);
  t.is(obj.tier, 2);
});
