const test = require('ava');

const breakpoint = require('../lib/breakpoint');

test('breakpoint works', (t) => {
  const point = breakpoint('raikou', 'kyogre');

  t.is(!!point.atk, true);
  t.is(!!point.def, true);

  const fiveDMG = point.atk[0].table[0];
  t.is(fiveDMG.dmg, 5);
  t.is(fiveDMG.lvl, 29);
});

test('breakpoint length is right', (t) => {
  const point = breakpoint('pikachu', 'flygon');
  t.is(point.atk[0].table.length, 1);
});
