const test = require('ava');

const genSnapshot = require('../lib/genSnapshot');
const dmgOutputSnapshot = require('./dmgOutputSnapshot.json');

test((t) => {
  t.deepEqual(genSnapshot().dmgOutputSnapshot, dmgOutputSnapshot);
});
