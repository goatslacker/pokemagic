const test = require('ava');

const simulateBattle = require('../lib/simulateBattle');

test((t) => {
  const stats = simulateBattle(
    [
      {
        iv: 0xfff,
        lvl: 40,
        name: 'exeggutor',
        move1: 'bullet seed',
        move2: 'solar beam',
      },
    ],
    [
      {
        iv: 0xfff,
        lvl: 40,
        name: 'gyarados',
        move1: 'waterfall',
        move2: 'hydro pump',
      },
    ],
    {}
  );

  t.is(stats.winner, 'atk');
  t.is(stats.timedOut, false);
  t.is(stats.timeElapsed, 27850);

  t.is(stats.state.atk[0].id, 'V0103');
  t.is(stats.state.atk[0].name, 'EXEGGUTOR');
  t.is(stats.state.atk[0].cp, 3014);
  t.is(stats.state.atk[0].dmgDealt, 364);

  t.is(stats.state.def[0].id, 'V0130');
  t.is(stats.state.def[0].name, 'GYARADOS');
  t.is(stats.state.def[0].cp, 3391);
  t.is(stats.state.def[0].dmgDealt, 163);

  t.is(Array.isArray(stats.log), true);

  t.is(stats.log[0].m, 'BULLET_SEED_FAST');
  t.is(stats.log[0].ms, 98450);
  t.is(stats.log[1].m, 'WATERFALL_FAST');
  t.is(stats.log[1].ms, 97450);
});

test('dodging', (t) => {
  const stats = simulateBattle(
    [
      {
        iv: 0xfff,
        lvl: 40,
        name: 'heracross',
        move1: 'counter',
        move2: 'close combat',
      },
    ],
    [
      {
        iv: 0xfff,
        lvl: 40,
        name: 'snorlax',
        move1: 'zen headbutt',
        move2: 'body slam',
      },
    ],
    {
      atkDodgeStrategy: 'charge',
    }
  );

  const dodges = stats.log.filter((x) => x.m === '@DODGE');
  t.is(dodges.length, 4);
});

test('friendship', (t) => {
  const stats = simulateBattle(
    [
      {
        iv: 0xeff,
        lvl: 24,
        name: 'zapdos',
        move1: 'charge beam',
        move2: 'thunderbolt',
      },
    ],
    [
      {
        name: 'wailmer',
        move1: 'splash',
        move2: 'body slam',
      },
    ],
    {
      friendship: 1,
      raid: true,
      raidTier: 1,
    }
  );

  const move = stats.log.find((x) => x.m === 'CHARGE_BEAM_FAST');
  t.is(move.dmg, 24);
});

// TODO battle party test
// TODO too many pokemon in battle party
// TODO charge move decisions tests
