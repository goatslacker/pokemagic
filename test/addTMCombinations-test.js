const test = require('ava');

const findPokemon = require('../lib/findPokemon');
const addTMCombinations = require('../lib/addTMCombinations');

test('adds legacy combinations', (t) => {
  const combos = addTMCombinations(findPokemon('dragonite'));
  const hasDracoMeteor = combos.some(({ B }) => B === 'DRACO_METEOR');
  t.is(hasDracoMeteor, true);
});

test('grimer has legacy', (t) => {
  const combos = addTMCombinations(findPokemon('grimer'));
  const hasAcid = combos.some(({ A }) => A === 'ACID_FAST');
  t.is(hasAcid, true);
});

test('alolan grimer does not have legacy moves', (t) => {
  const combos = addTMCombinations(findPokemon('grimer_alola'));
  const hasAcid = combos.some(({ A }) => A === 'ACID_FAST');
  t.is(hasAcid, false);
});

test('generates correct amount of moves', (t) => {
  const combos = addTMCombinations(findPokemon('gyarados'));
  t.is(combos.length, 20);
});

test('legacy enum', (t) => {
  const combos = addTMCombinations(findPokemon('gyarados'));

  const current = combos.find(
    ({ A, B }) => A === 'WATERFALL_FAST' && B === 'HYDRO_PUMP'
  );
  t.is(current.legacy, 0);

  const singleLegacyQuick = combos.find(
    ({ A, B }) => A === 'DRAGON_TAIL_FAST' && B === 'HYDRO_PUMP'
  );
  t.is(singleLegacyQuick.legacy, 1);

  const singleLegacyCharge = combos.find(
    ({ A, B }) => A === 'WATERFALL_FAST' && B === 'TWISTER'
  );
  t.is(singleLegacyCharge.legacy, 2);

  const doubleLegacy = combos.find(
    ({ A, B }) => A === 'DRAGON_BREATH_FAST' && B === 'DRAGON_PULSE'
  );
  t.is(doubleLegacy.legacy, 3);
});

test('community day', (t) => {
  const combos = addTMCombinations(findPokemon('tyranitar'));
  const cdMoves = combos.find(
    ({ A, B }) => A === 'SMACK_DOWN_FAST' && B === 'STONE_EDGE'
  );
  t.is(cdMoves.legacy, 5);
});

test('venomoth', (t) => {
  const combos = addTMCombinations(findPokemon('venomoth'));
  const hasEmpty = combos.some(({ B }) => B === '');
  t.is(hasEmpty, false);
});
