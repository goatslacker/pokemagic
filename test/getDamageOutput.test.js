const test = require('ava');

const findMove = require('../lib/findMove');
const findPokemon = require('../lib/findPokemon');
const getDamageOutput = require('../lib/getDamageOutput');

test('machamp', (t) => {
  const dmg = getDamageOutput(
    {
      pokemon: findPokemon('machamp'),
      quick: findMove('counter'),
      charge: findMove('dynamicpunch'),
    },
    {
      pokemon: findPokemon('bliss'),
      quick: findMove('zenheadbutt'),
      charge: findMove('dazzling'),
    }
  );

  t.is(dmg.dps, 24.311);
  t.is(dmg.tdo, 632);
  t.is(dmg.score, 15364.2);
});

test('entei', (t) => {
  const dmg = getDamageOutput(
    {
      pokemon: findPokemon('entei'),
      quick: findMove('firespin'),
      charge: findMove('overheat'),
    },
    {
      pokemon: findPokemon('bliss'),
      quick: findMove('zenheadbutt'),
      charge: findMove('dazzling'),
    }
  );

  t.is(dmg.dps, 16.781);
  t.is(dmg.tdo, 1008.6);
  t.is(dmg.score, 16925.1);
});
