const Levels = require('../json/levels');
const getDMG = require('./getDMG');
const parseIV = require('./parseIV');

function getCounterDPS(defender, attacker, options = {}) {
  const atkIV = parseIV(attacker.iv || 0xfff);
  const defIV = parseIV(defender.iv || 0xfff);

  const atkCpm = Levels[attacker.lvl || 40];
  const defCpm = options.raidInfo
    ? options.raidInfo.cpm
    : Levels[defender.lvl || 40];

  const moveArgs = {
    Atk: (defender.pokemon.stats.attack + defIV.atk) * defCpm,
    Def: (attacker.pokemon.stats.defense + atkIV.def) * atkCpm,
    attacker: defender.pokemon,
    defender: attacker.pokemon,
    weatherConditions: options.weather || 'EXTREME',
  };
  const moveDelay = 2000;

  const quickDmg = getDMG(defender.quick, moveArgs);
  const quickDuration = (defender.quick.DurationMs + moveDelay) / 1000;

  const chargeDmg = getDMG(defender.charge, moveArgs);
  const chargeDuration = (defender.charge.DurationMs + moveDelay) / 1000;
  const chargeEnergy = -defender.charge.Energy || 0;

  const n = Math.max(1, (3 * chargeEnergy) / 100);

  return {
    x:
      (-attacker.charge.Energy || 0) * 0.5 +
      (attacker.quick.Energy || 1) * 0.5 +
      (0.5 * (n * quickDmg + chargeDmg)) / (n + 1),
    y: (n * quickDmg + chargeDmg) / (n * quickDuration + chargeDuration),
  };
}

// Ref: https://pokemongo.gamepress.gg/tdo-how-calculate-pokemons-ability
// attacker = {
//   pokemon: Pokemon
//   quick: Move
//   charge: Move
// }
function getDamageOutput(attacker, defender, options = {}) {
  const atkIV = parseIV(attacker.iv || 0xfff);
  const defIV = parseIV(defender.iv || 0xfff);

  const atkCpm = Levels[attacker.lvl || 40];
  const defCpm = options.raidInfo
    ? options.raidInfo.cpm
    : Levels[defender.lvl || 40];

  const { x, y } = getCounterDPS(defender, attacker, options);
  const Sta = (attacker.pokemon.stats.stamina + atkIV.sta) * atkCpm;

  const moveArgs = {
    Atk: (attacker.pokemon.stats.attack + atkIV.atk) * atkCpm,
    Def: (defender.pokemon.stats.defense + defIV.def) * defCpm,
    attacker: attacker.pokemon,
    defender: defender.pokemon,
    friendshipLevel: options.friendship,
    weatherConditions: options.weather || 'EXTREME',
  };

  const moveDelay = options.isGymAttacker ? 2000 : 0;

  const quickDmg = getDMG(attacker.quick, moveArgs);
  const quickDuration = (attacker.quick.DurationMs + moveDelay) / 1000;
  const quickEnergy = attacker.quick.Energy || 1;

  const chargeDmg = getDMG(attacker.charge, moveArgs);
  const chargeDmgWindowStart = attacker.charge.DamageWindowStartMs / 1000;
  const chargeDuration = (attacker.charge.DurationMs + moveDelay) / 1000;

  let chargeEnergy = -attacker.charge.Energy || 0;
  if (chargeEnergy >= 100) {
    chargeEnergy =
      chargeEnergy + 0.5 * quickEnergy + 0.5 * y * chargeDmgWindowStart;
  }

  const quickDPS = quickDmg / quickDuration;
  const quickEnergyPS = quickEnergy / quickDuration;
  const chargeDPS = chargeDmg / chargeDuration;
  const chargeEnergyPS = chargeEnergy / chargeDuration;

  const stamina = Sta / y;

  let dps =
    (quickDPS * chargeEnergyPS + chargeDPS * quickEnergyPS) /
      (chargeEnergyPS + quickEnergyPS) +
    ((chargeDPS - quickDPS) / (chargeEnergyPS + quickEnergyPS)) *
      (1 / 2 - x / Sta) *
      y;
  dps = Math.max(Math.min(dps, chargeDPS), quickDPS);

  const tdo = dps * stamina;

  return {
    dps: Math.round(dps * 1000) / 1000,
    score: Math.round(dps * tdo * 10) / 10,
    tdo: Math.round(tdo * 10) / 10,
  };
}

module.exports = getDamageOutput;
