const effectivenessList = require('./effectivenessList');
const estimateMultipleBattles = require('./estimateMultipleBattles');
const findMove = require('./findMove');
const getRaidInfo = require('./raid');
const getTypeEffectiveness = require('./getTypeEffectiveness');
const isLegendary = require('./isLegendary');

function defenderProfile({
  chargeMoveName,
  filterAttackerMoveset,
  numPokemon,
  pokemon,
  quickMoveName,
  raid = false,
  raidTier = 3,
  scoring = 'win',
  weather = 'EXTREME',
  ...restOptions
}) {
  const isRaid = raid || isLegendary(pokemon.name);

  let filterDefenderMoveset = () => true;
  if (quickMoveName && chargeMoveName) {
    const quickMove = findMove(quickMoveName).Name;
    const chargeMove = findMove(chargeMoveName).Name;
    filterDefenderMoveset = (x) => x.A === quickMove && x.B === chargeMove;
  }

  const raidInfo = isRaid ? getRaidInfo(pokemon, raidTier) : null;

  function filterBadMovesets({ B }) {
    return getTypeEffectiveness(pokemon, findMove(B)) >= 1;
  }

  const data = estimateMultipleBattles(pokemon, {
    ...restOptions,
    filterAttackerMoveset: filterAttackerMoveset || filterBadMovesets,
    filterDefenderMoveset,
    isAttacker: false,
    numPokemon: numPokemon || (quickMoveName && chargeMoveName ? 3 : 5),
    raidInfo,
    scoring,
    weather,
  });

  const types = effectivenessList(pokemon);

  return {
    data,
    pokemon,
    types,
  };
}

module.exports = defenderProfile;
