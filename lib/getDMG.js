const getTypeEffectiveness = require('./getTypeEffectiveness');
const weather = require('./weather');

const friends = [1, 1.03, 1.05, 1.07, 1.1];

// Reference
// https://pokemongo.gamepress.gg/damage-mechanics
function getDMG(
  move,
  { Atk, Def, attacker, defender, friendshipLevel, weatherConditions }
) {
  const power = move.Power || 0;
  const stab =
    move.Type === (attacker.type1 || move.Type === attacker.type2) ? 1.2 : 1;
  const eff = getTypeEffectiveness(defender, move);
  const wab = weather(weatherConditions || 'EXTREME', move.Type);
  const fab = friends[friendshipLevel || 0];

  const multipliers = stab * eff * wab * fab;
  return Math.floor(((0.5 * power * Atk) / Def) * multipliers) + 1;
}

module.exports = getDMG;
