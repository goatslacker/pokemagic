const addTMCombinations = require('./addTMCombinations');
const findMove = require('./findMove');
const getDamageOutput = require('./getDamageOutput');
const topPokemon = require('./topPokemon');

function sortByScore(a, b) {
  // Pick the highest score
  if (a.score !== b.score) {
    return a.score > b.score ? -1 : 1;
  }

  // Otherwise it's a tie
  return 0;
}

// The list returned by getCounters is flat
// This function groups all same Pokemon into buckets
function reduceListIntoBuckets(counters, maxPokemon) {
  return counters.map((result) => {
    let pokemonFound = 0;
    const index = {};
    const pokemon = {
      quick: result.quick,
      charge: result.charge,
      results: [],
    };

    result.results.every((stats) => {
      const { id, name } = stats;

      if (!index.hasOwnProperty(id)) {
        index[id] = pokemonFound;
        pokemon.results[index[id]] = {
          id,
          name,
          stats: [],
        };
        pokemonFound += 1;
      }

      pokemon.results[index[id]].stats.push(stats);

      return pokemonFound < maxPokemon;
    });

    return pokemon;
  });
}

function estimateMultipleBattles(pokemon, optionsOverride) {
  const options = Object.assign(
    {
      filterAttackerMoveset: () => true,
      filterDefenderMoveset: () => true,
      filterOpponents: () => true,
      isAttacker: true,
      numPokemon: 5,
      opponents: topPokemon,
      raidInfo: null,
      scoring: 'win', // How do we score matches? [dps, tdo, win]
      weather: 'EXTREME',
    },
    optionsOverride
  );

  // Format
  // [
  //   {
  //     A: 'QuickMove',
  //     B: 'ChargeMove',
  //     legacy: Enum,
  //   },
  // ]
  const counters = {};

  const pokemonMoves = addTMCombinations(pokemon).filter(
    options.isAttacker
      ? options.filterAttackerMoveset
      : options.filterDefenderMoveset
  );

  pokemonMoves.forEach(({ A, B }) => {
    const key = `${A}/${B}`;
    counters[key] = [];
  });

  const opponents = options.opponents.filter(options.filterOpponents);

  opponents.forEach((opponent) => {
    const opponentMoves = addTMCombinations(opponent).filter(
      options.isAttacker
        ? options.filterDefenderMoveset
        : options.filterAttackerMoveset
    );

    pokemonMoves.forEach((pokeMoves) => {
      const key = `${pokeMoves.A}/${pokeMoves.B}`;
      const pokemonObject = {
        pokemon,
        quick: findMove(pokeMoves.A),
        charge: findMove(pokeMoves.B),
      };

      opponentMoves.forEach((oppMoves) => {
        const opponentObject = {
          pokemon: opponent,
          quick: findMove(oppMoves.A),
          charge: findMove(oppMoves.B),
        };

        const attacker = options.isAttacker ? pokemonObject : opponentObject;
        const defender = options.isAttacker ? opponentObject : pokemonObject;

        const dmg = getDamageOutput(attacker, defender, {
          isRaid: options.raidInfo !== null,
          raidInfo: options.raidInfo,
          weather: options.weather,
        });

        const score =
          options.scoring === 'dps'
            ? dmg.dps
            : options.scoring === 'tdo'
              ? dmg.tdo
              : dmg.score;

        const stats = {
          dps: dmg.dps,
          id: opponent.id,
          legacy: oppMoves.legacy,
          moves: [oppMoves.A, oppMoves.B],
          name: opponent.name,
          score,
          tdo: dmg.tdo,
          total: dmg.score,
        };

        counters[key].push(stats);
      });
    });
  });

  const counterKeys = Object.keys(counters);
  if (!counterKeys.length) return null;

  const flatList = counterKeys.map((key) => {
    const [quick, charge] = key.split('/');
    return {
      quick,
      charge,
      results: counters[key].sort(sortByScore),
    };
  });

  return reduceListIntoBuckets(flatList, options.numPokemon);
}

module.exports = estimateMultipleBattles;
