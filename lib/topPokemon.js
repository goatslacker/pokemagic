const Pokemon = require('../json/pokemon');
const getMaxCP = require('./getMaxCP');

const NOT_ALL_THAT = new Set(['V0151', 'V0386_ATTACK', 'V0386_DEFENSE']);

const topPokemon = Pokemon.map((poke) => ({
  poke,
  cp: getMaxCP(poke),
}))
  .filter((x) => !NOT_ALL_THAT.has(x.poke.id))
  .sort((a, b) => (a.cp > b.cp ? -1 : 1))
  .slice(0, 100)
  .map((x) => x.poke);

module.exports = topPokemon;
