const getDamageOutputFromKey = require('./getDamageOutputFromKey');
const simulateBattle = require('./simulateBattle');
const topPokemon = require('./topPokemon');

// vs every other top pokemon
const testCases = topPokemon.map((def) => {
  return {
    // Mewtwo : Confusion / Shadow Ball
    atk: 'V0150.40.fff.1.1',
    def: `${def.id}.40.fff.0.0`,
  };
});

function snapshots() {
  const dmgOutputSnapshot = testCases.map(({ atk, def }) => {
    const key = `${atk}v${def}`;
    return {
      key,
      data: getDamageOutputFromKey(key),
    };
  });

  const battleSnapshot = testCases.map(({ atk, def }) => {
    const key = `${atk}v${def}`;
    const data = simulateBattle.fromKey(key);
    return {
      key,
      // grab the first 20 events in the log
      log20: data.log.slice(0, 20),
    };
  });

  return {
    battleSnapshot,
    dmgOutputSnapshot,
  };
}

module.exports = snapshots;
