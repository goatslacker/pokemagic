const effectivenessList = require('./effectivenessList');
const estimateMultipleBattles = require('./estimateMultipleBattles');
const findMove = require('./findMove');
const isLegendary = require('./isLegendary');

function attackerProfile({
  chargeMoveName,
  numPokemon = 20,
  pokemon,
  quickMoveName,
  scoring = 'dps',
  weather = 'EXTREME',
}) {
  let filterAttackerMoveset = () => true;
  if (quickMoveName && chargeMoveName) {
    const quickMove = findMove(quickMoveName).Name;
    const chargeMove = findMove(chargeMoveName).Name;
    filterAttackerMoveset = (x) => x.A === quickMove && x.B === chargeMove;
  }

  const data = estimateMultipleBattles(pokemon, {
    filterAttackerMoveset,
    filterOpponents: (x) => !isLegendary(x.name),
    numPokemon,
    scoring,
    weather,
  });

  const types = effectivenessList(pokemon);

  return {
    data,
    pokemon,
    types,
  };
}

module.exports = attackerProfile;
