const findMove = require('./findMove');
const findPokemon = require('./findPokemon');
const getDamageOutput = require('./getDamageOutput');

function expandKey(key) {
  const [id, lvl, iv, quickId, chargeId] = key.split('.');
  const pokemon = findPokemon(id);

  return {
    charge: findMove(pokemon.moves.charge[chargeId]),
    iv: parseInt(iv, 16),
    lvl,
    pokemon,
    quick: findMove(pokemon.moves.quick[quickId]),
  };
}

function getDamageOutputFromKey(key, options = {}) {
  const [atk, def] = key.split('v');
  return getDamageOutput(expandKey(atk), expandKey(def), options);
}

module.exports = getDamageOutputFromKey;
