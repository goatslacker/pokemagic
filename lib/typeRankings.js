const Levels = require('../json/levels');
const Moves = require('../json/moves');
const addTMCombinations = require('./addTMCombinations');
const findMove = require('./findMove');
const getDamageOutput = require('./getDamageOutput');
const getHP = require('./getHP');
const getTypeEffectiveness = require('./getTypeEffectiveness');
const topPokemon = require('./topPokemon');

const ALL_MOVES = {};
topPokemon.forEach((poke) => {
  ALL_MOVES[poke.id] = addTMCombinations(poke);
});

function sortByScore(a, b) {
  if (a.score !== b.score) {
    return a.score > b.score ? -1 : 1;
  }
  return 0;
}

function createTally() {
  return {
    count: 0,
    dps: 0,
    score: 0,
    tdo: 0,
    wins: 0,
  };
}

function reduceScore(acc, next) {
  return {
    count: acc.count + 1,
    dps: acc.dps + next.dps,
    score: acc.score + next.score,
    tdo: acc.tdo + next.tdo,
    wins: acc.wins + (next.won ? 1 : 0),
  };
}

function tallyResults(battleStats) {
  const movesets = {};

  battleStats.forEach((battle) => {
    const key = battle.moves.join('/');
    if (!movesets[key]) movesets[key] = [];
    movesets[key].push(battle);
  });

  return Object.keys(movesets)
    .map((movesKey) => {
      const moves = movesKey.split('/');
      const result = movesets[movesKey];
      const total = result.reduce(reduceScore, createTally());
      return {
        dps: Math.round((total.dps / result.length) * 100) / 100,
        moves,
        score: Math.round((total.score / result.length) * 1000) / 1000,
        tdo: Math.round((total.tdo / result.length) * 100) / 100,
        total,
        wins: total.wins,
      };
    })
    .sort(sortByScore);
}

function typeRankings(stype, { weather = 'EXTREME' } = {}) {
  const type = stype.toUpperCase();

  // Find all pokemon in the top 100 that have that type
  const pokesOfType = topPokemon.filter(
    (poke) => poke.type1 === type || poke.type2 === type
  );

  // Fight each pokemon vs opponents that are weak/neutral to the type.
  const weakPokemon = topPokemon.filter(
    (poke) => getTypeEffectiveness(poke, { Type: type }) >= 1
  );

  const Cache = {};

  return pokesOfType
    .map((pokeAtk) => {
      Cache[pokeAtk.id] = [];

      // Filter by move combinations where the charge move is same type.
      const movesThatWork = ALL_MOVES[pokeAtk.id].filter(
        (atkMoves) => Moves[atkMoves.B].Type === type
      );

      movesThatWork.forEach((atkMoves) => {
        weakPokemon.forEach((pokeDef) => {
          // We'll fight our Pokemon vs all of the opponent's moveset
          // combinations so we can get a fair assessment.
          // The fight is between two 100% level 40 Pokemon.
          ALL_MOVES[pokeDef.id].forEach((defMoves) => {
            const dmg = getDamageOutput(
              {
                pokemon: pokeAtk,
                quick: findMove(atkMoves.A),
                charge: findMove(atkMoves.B),
              },
              {
                pokemon: pokeDef,
                quick: findMove(defMoves.A),
                charge: findMove(defMoves.B),
              },
              {
                weather,
              }
            );

            const fullHP = getHP(pokeDef, 15, Levels[40]) * 2;

            const stats = {
              dps: dmg.dps,
              moves: [atkMoves.A, atkMoves.B],
              name: pokeAtk.name,
              id: pokeAtk.id,
              score: dmg.score,
              tdo: dmg.tdo,
              won: dmg.tdo > fullHP,
              vs: pokeDef.id,
            };

            Cache[pokeAtk.id].push(stats);
          });
        });
      });

      if (Cache[pokeAtk.id].length === 0) {
        return null;
      }

      // Tally results...
      const results = tallyResults(Cache[pokeAtk.id]);

      // Only use the Pokemon's best moveset
      const best = results[0];

      return Object.assign(
        {
          name: pokeAtk.name,
          id: pokeAtk.id,
        },
        best
      );
    })
    .filter(Boolean)
    .sort(sortByScore);
}

module.exports = typeRankings;
