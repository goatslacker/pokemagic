/* eslint-disable no-param-reassign */

const Levels = require('../json/levels');
const findMove = require('./findMove');
const findPokemon = require('./findPokemon');
const getCP = require('./getCP');
const getDMG = require('./getDMG');
const getHP = require('./getHP');
const isLegendary = require('./isLegendary');
const parseIV = require('./parseIV');
const raid = require('./raid');

// 100 seconds in ms.
const TIME_LIMIT = 100000;
// CPU's first attacks are every second.
const DEF_GYM_FIRST_ATTACKS = 1000;
// CPU attacks about every 2 seconds.
const DEF_GYM_ATTACK_TIME = 2000;
// Level 40.
const MAX_LEVEL = 40;

const DODGE_DURATION = 500;

const ATK_DELAY = 700;
const DEF_DELAY = 1600;

function getDMGCombo(quick, charge, options) {
  return {
    quick: getDMG(quick, options),
    charge: getDMG(charge, options),
  };
}

function cacheDMG(pokeAtk, pokeDef, raidInfo, options) {
  const atkECpM = Levels[pokeAtk.lvl || MAX_LEVEL];
  // Can be overwritten for raids
  const defECpM = raidInfo.cpm || Levels[pokeDef.lvl || MAX_LEVEL];

  const atkIV = parseIV(pokeAtk.iv || 0xfff);
  const defIV = parseIV(pokeDef.iv || 0xfff);

  return getDMGCombo(pokeAtk.quickMove, pokeAtk.chargeMove, {
    Atk: (pokeAtk.pokemon.stats.attack + atkIV.atk) * atkECpM,
    Def: (pokeDef.pokemon.stats.defense + defIV.def) * defECpM,
    attacker: pokeAtk.pokemon,
    defender: pokeDef.pokemon,
    friendshipLevel: options.friendship,
    weatherConditions: options.weather,
  });
}

// The Pokemon's state which keeps track of a Pokemon's running HP, Energy,
// how much damage they've dealt, and other information.
// TODO this should be a class so we can create and store these
function createState(poke, timeCanAttackMs, totalDMG = 0) {
  const { quickMove, chargeMove } = poke;
  const cooldownQuick = quickMove.DurationMs - quickMove.DamageWindowStartMs;
  const cooldownCharge = chargeMove.DurationMs - chargeMove.DamageWindowStartMs;
  const chargeMoveLimit = Math.abs(chargeMove.Energy);
  const iv = parseIV(poke.iv || 0xfff);
  const fullHP = getHP(poke.pokemon, iv.sta, Levels[poke.lvl || 40]);

  return {
    chargeMove,
    chargeMoveLimit,
    cp: getCP(poke.pokemon, poke.iv || 0xfff, Levels[poke.lvl || 40]),
    cooldownCharge,
    cooldownQuick,
    energy: 0,
    fullHP,
    hp: fullHP,
    maxEnergy: 100,
    nextTurnMs: timeCanAttackMs - quickMove.DamageWindowStartMs,
    id: poke.pokemon.id,
    pokemon: poke,
    quickMove,
    totalDMG,
    turnCounter: 0,
    useCharge: false,
  };
}

function crushState(state) {
  const txtIV = state.pokemon.iv || 0xfff;
  const lvl = Number(state.pokemon.lvl) || 40;
  const iv = parseIV(txtIV);
  const { id } = state;
  const moveKeys = [
    state.pokemon.pokemon.moves.quick.indexOf(state.pokemon.quickMove.Name),
    state.pokemon.pokemon.moves.charge.indexOf(state.pokemon.chargeMove.Name),
  ];

  return {
    key: `${id}.${lvl}.${txtIV.toString(16)}.${moveKeys[0]}.${moveKeys[1]}`,
    id,
    name: state.pokemon.pokemon.name,
    lvl,
    iv: `${iv.atk}/${iv.def}/${iv.sta}`,
    moves: [state.pokemon.quickMove.Name, state.pokemon.chargeMove.Name],
    cp: state.cp,
    hp: state.fullHP,
    // dmg dealt is cummulative for a defender
    dmgDealt: state.totalDMG,
    // dmg taken is capped at your HP
    dmgTaken: Math.min(state.fullHP, state.fullHP - state.hp),
  };
}

// When an end-fight condition is met this function returns the result.
function getResult({ log, state, timeLimit, timeRemaining, winner }) {
  return {
    log,
    state: {
      atk: state.attackers.map(crushState),
      def: state.defenders.map(crushState),
    },
    timeElapsed: timeLimit - timeRemaining,
    timeRemaining,
    timedOut: timeRemaining <= 0,
    winner,
  };
}

function dodgeNothing(move) {
  return move.Damage;
}

function dodgeAttackPerfect(move) {
  return Math.max(1, Math.floor(0.25 * move.Damage));
}

function dodgeChargePerfect(move) {
  return move.Energy > 0 ? dodgeNothing(move) : dodgeAttackPerfect(move);
}

// Applies the damage and adds the resulting event to the combat log.
function hitAndLog({
  AttackerState,
  DefenderState,
  dodgeModifier,
  label,
  log,
  move,
  options,
  timeRemaining,
}) {
  const dmg = dodgeModifier(move);
  const dmgDealt = DefenderState.hp - dmg > 0 ? dmg : DefenderState.hp;
  const didDodge = dmg < move.Damage;

  // On odd turns and where the dmg is odd we'll +1 the energy gains
  // because defenders gain 1 energy for every -2hp
  const plusOneEnergy =
    dmg % 2 === 1 && AttackerState.turnCounter % 2 === 1 ? 1 : 0;
  const defEnergyGain = Math.floor(dmgDealt / 2) + plusOneEnergy;

  AttackerState.totalDMG += dmgDealt;
  AttackerState.energy = Math.min(
    AttackerState.maxEnergy,
    AttackerState.energy + move.Energy
  );

  DefenderState.energy = Math.min(
    DefenderState.maxEnergy,
    DefenderState.energy + defEnergyGain
  );
  DefenderState.hp -= dmgDealt;

  if (!options.skipLog) {
    log.push({
      p: label,
      m: move.Name,
      dmg: dmgDealt,
      ms: timeRemaining,
      a: {
        e: AttackerState.energy,
        h: AttackerState.hp,
      },
      d: {
        e: DefenderState.energy,
        h: DefenderState.hp,
      },
    });

    if (didDodge) {
      log.push({
        p: 'atk',
        m: '@DODGE',
        dmg: null,
        ms: timeRemaining,
      });
    }
  }

  return {
    didDodge,
  };
}

// Determines if a Player should attack or not then performs the attack that
// it has been instructed to land. If the Defending Player's Pokemon loses all
// HP then the result is returned.
function landAttack({
  AttackerState,
  DefenderState,
  label,
  log,
  options,
  timeRemaining,
}) {
  const shouldAttack = timeRemaining === AttackerState.nextTurnMs;
  if (!shouldAttack) return null;

  let dodgeModifier = dodgeNothing;
  if (label === 'def') {
    if (options.atkDodgeStrategy === 'charge') {
      dodgeModifier = dodgeChargePerfect;
    } else if (options.atkDodgeStrategy === 'all') {
      dodgeModifier = dodgeAttackPerfect;
    }
  }

  const { dmg } = AttackerState;

  const quickAttack = {
    Name: AttackerState.quickMove.Name,
    Damage: dmg.quick,
    Energy: AttackerState.quickMove.Energy,
  };
  const chargeAttack = {
    Name: AttackerState.chargeMove.Name,
    Damage: dmg.charge,
    Energy: AttackerState.chargeMove.Energy,
  };

  // Perform the DMG and add it to the combat log
  const { didDodge } = hitAndLog({
    AttackerState,
    DefenderState,
    dodgeModifier,
    label,
    log,
    move: AttackerState.useCharge ? chargeAttack : quickAttack,
    options,
    timeRemaining,
  });

  // Win condition
  if (DefenderState.hp <= 0) {
    return { ko: true };
  }

  // Increment the turn counter which is used by the defender to know
  // whether or not they'll use their charge move
  AttackerState.turnCounter += 1;
  return { ms: didDodge ? DODGE_DURATION : 0 };
}

function setAttackerNextTurn(state, timeRemaining, extraMs = 0) {
  // Set the attacker's intention to use a charge move next turn.
  state.useCharge = state.energy >= state.chargeMoveLimit;

  state.nextTurnMs = state.useCharge
    ? timeRemaining -
      state.chargeMove.DamageWindowStartMs -
      state.cooldownCharge -
      extraMs
    : timeRemaining -
      state.quickMove.DamageWindowStartMs -
      state.cooldownQuick -
      extraMs;
}

function setDefenderNextTurn(
  DefenderState,
  timeRemaining,
  options,
  extraMs = 0
) {
  // The defender attacks every 2 seconds except for the first two turns
  // where it attacks each second.
  if (DefenderState.turnCounter < 2) {
    DefenderState.nextTurnMs = DefenderState.useCharge
      ? timeRemaining - DEF_GYM_FIRST_ATTACKS - extraMs
      : timeRemaining - DEF_GYM_FIRST_ATTACKS - extraMs;
  } else {
    const gymAttackTime = options.random
      ? Math.random() + 1.5
      : DEF_GYM_ATTACK_TIME;

    DefenderState.nextTurnMs = DefenderState.useCharge
      ? timeRemaining -
        DefenderState.chargeMove.DamageWindowStartMs -
        DefenderState.cooldownCharge -
        gymAttackTime -
        extraMs
      : timeRemaining -
        DefenderState.quickMove.DamageWindowStartMs -
        DefenderState.cooldownQuick -
        gymAttackTime -
        extraMs;
  }
}

function key2PBO(poke) {
  const { key } = poke;

  const [id, lvl, iv, quickId, chargeId] = key.split('.');
  const pokemon = findPokemon(id);

  return {
    pokemon,
    quickMove: findMove(pokemon.moves.quick[quickId]),
    chargeMove: findMove(pokemon.moves.charge[chargeId]),
    lvl,
    iv: parseInt(iv, 16),
  };
}

function resolvePokemonBattleObject(poke) {
  if (poke.key) {
    return key2PBO(poke);
  }
  if (!poke.pokemon) {
    poke.pokemon = findPokemon(poke.name);
  }
  if (!poke.quickMove) {
    poke.quickMove = findMove(poke.move1);
  }
  if (!poke.chargeMove) {
    poke.chargeMove = findMove(poke.move2);
  }
  return poke;
}

// Reference
// https://pokemongo.gamepress.gg/gym-combat-mechanics
function simulateBattle(pokeAtkArr, pokeDefArr, options) {
  const Attackers = pokeAtkArr.map(resolvePokemonBattleObject);
  const Defenders = pokeDefArr.map(resolvePokemonBattleObject);

  if (Attackers.length > 6 || Defenders.length > 6) {
    throw new ReferenceError('Maximum battle party is 6');
  }

  let pokeAtk = Attackers.shift();
  let pokeDef = Defenders.shift();

  const isRaid =
    typeof options.raid === 'boolean'
      ? options.raid
      : isLegendary(pokeDef.pokemon.name);
  const raidInfo = isRaid ? raid(pokeDef.pokemon, options.raidTier) : {};
  const dmgCacheDefenderOptions = { weather: options.weather };

  // This is where the battle happens.
  //
  // The battle goes on until there is a winner or until the time limit of 100
  // seconds is reached. Each step represents a user's turn.
  const log = [];
  const state = {
    attackers: [],
    defenders: [],
  };

  const timeLimit = TIME_LIMIT * (isRaid ? 1.8 : 1);
  let timeRemaining = timeLimit;

  // Attacker can go after 700ms.
  let AttackerState = createState(pokeAtk, timeLimit - ATK_DELAY);
  AttackerState.dmg = cacheDMG(pokeAtk, pokeDef, raidInfo, options);
  state.attackers.push(AttackerState);

  // Defender can go after 1600ms.
  // The Defender's HP can be overwritten in case of a raid.
  let DefenderState = createState(pokeDef, timeLimit - DEF_DELAY);
  DefenderState.dmg = cacheDMG(
    pokeDef,
    pokeAtk,
    raidInfo,
    dmgCacheDefenderOptions
  );
  state.defenders.push(DefenderState);

  // Set the Defender's HP to either the Raid defined amount or double.
  DefenderState.fullHP = raidInfo.hp || DefenderState.fullHP * 2;
  DefenderState.cp = raidInfo.cp || DefenderState.cp;
  DefenderState.hp = DefenderState.fullHP;
  // Max energy allotted is doubled
  DefenderState.maxEnergy *= 2;

  let defenderUseChargeNext = false;

  // Simulate the battle!
  for (timeRemaining; timeRemaining > 0; timeRemaining -= 10) {
    const defTurn = timeRemaining === DefenderState.nextTurnMs;

    // CPU charge move usage:
    // 50% chance when energy is sufficient
    // decided at the beginning of the previous move
    if (defTurn) {
      // If we want determinism a defender has a 50% chance of using a charge
      // move when ready so we'll just average it and say it'll use its
      // charge on every odd turn.
      defenderUseChargeNext = options.random
        ? Math.random() > 0.5
        : DefenderState.turnCounter % 2 !== 0;
    }

    // Attacking Player goes first.
    const winAtk = landAttack({
      AttackerState,
      DefenderState,
      label: 'atk',
      log,
      options,
      timeLimit,
      timeRemaining,
    });

    if (winAtk) {
      if (winAtk.ko) {
        if (!options.skipLog) {
          log.push({
            p: 'def',
            m: '@FAINT',
            dmg: null,
            ms: timeRemaining,
          });
        }

        if (!Defenders.length) {
          return getResult({
            log,
            state,
            timeLimit,
            timeRemaining,
            winner: 'atk',
          });
        }

        pokeDef = Defenders.shift();
        DefenderState = createState(pokeDef, timeRemaining - 1000);
        DefenderState.dmg = cacheDMG(
          pokeDef,
          pokeAtk,
          raidInfo,
          dmgCacheDefenderOptions
        );
        state.defenders.push(DefenderState);
        if (!options.skipLog) {
          log.push({
            p: 'def',
            m: '@SWITCH',
            dmg: null,
            ms: timeRemaining,
          });
        }
      }

      setAttackerNextTurn(AttackerState, timeRemaining, winAtk.ms);
    }

    // Make sure that the defender has enough energy available now in order
    // to fire off the charge
    if (DefenderState.useCharge) {
      const hasEnoughEnergy =
        DefenderState.energy >= DefenderState.chargeMoveLimit;
      DefenderState.useCharge = hasEnoughEnergy;
    }

    // Defending Player goes next.
    const winDef = landAttack({
      // Flipped here because the Defender is attacking the Attacker.
      AttackerState: DefenderState,
      DefenderState: AttackerState,
      label: 'def',
      log,
      options,
      timeLimit,
      timeRemaining,
    });

    if (winDef) {
      if (winDef.ko) {
        if (!options.skipLog) {
          log.push({
            p: 'atk',
            m: '@FAINT',
            dmg: null,
            ms: timeRemaining,
          });
        }

        if (!Attackers.length) {
          return getResult({
            log,
            state,
            timeLimit,
            timeRemaining,
            winner: 'def',
          });
        }

        pokeAtk = Attackers.shift();
        AttackerState = createState(pokeAtk, timeRemaining - 1000);
        AttackerState.dmg = cacheDMG(pokeAtk, pokeDef, raidInfo, options);
        state.attackers.push(AttackerState);
        if (!options.skipLog) {
          log.push({
            p: 'atk',
            m: '@SWITCH',
            dmg: null,
            ms: timeRemaining,
          });
        }
      }

      setDefenderNextTurn(DefenderState, timeRemaining, options, winDef.ms);
      DefenderState.useCharge = defenderUseChargeNext;
    }
  }

  if (!options.skipLog) {
    log.push({
      p: 'atk',
      m: '@TIME_OUT',
      dmg: null,
      ms: timeRemaining,
    });
  }

  // The battle timed out :(
  return getResult({
    log,
    state,
    timeLimit,
    timeRemaining,
    winner: 'def',
  });
}

function simulateBattleFromKey(key, options = {}) {
  const [atk, def] = key.split('v');
  return simulateBattle([{ key: atk }], [{ key: def }], options);
}

simulateBattle.fromKey = simulateBattleFromKey;

module.exports = simulateBattle;
