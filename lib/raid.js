const raidCPM = require('../json/raid-cpm');

const tiers = [null, 600, 1800, 3000, 7500, 12500];
const dmgCPM = [null, 0.61, 0.67, 0.73, 0.79, 0.79];

function raid(poke, customTier) {
  if (!poke) return {};

  if (!customTier && raidCPM[poke.name]) {
    return raidCPM[poke.name];
  }

  const tier = customTier || 3;

  const BaseAtt = poke.stats.attack;
  const BaseDef = poke.stats.defense;
  const hp = tiers[tier];
  const cpm = dmgCPM[tier];

  const cp = Math.floor(
    ((BaseAtt + 15) * Math.sqrt(BaseDef + 15) * Math.sqrt(hp)) / 10
  );
  return { cp, hp, cpm, tier };
}

module.exports = raid;
