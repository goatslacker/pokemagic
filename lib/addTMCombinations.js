const Legacy = require('../json/legacy');

const communityDay = {
  V0003: 'FRENZY_PLANT',
  V0006: 'BLAST_BURN',
  V0009: 'HYDRO_CANNON',
  V0133: 'LAST_RESORT',
  V0134: 'LAST_RESORT',
  V0135: 'LAST_RESORT',
  V0136: 'LAST_RESORT',
  V0149: 'DRACO_METEOR',
  V0154: 'FRENZY_PLANT',
  V0157: 'BLAST_BURN',
  V0181: 'DRAGON_PULSE',
  V0196: 'LAST_RESORT',
  V0197: 'LAST_RESORT',
  V0248: 'SMACK_DOWN_FAST',
  V0376: 'METEOR_MASH',
};

const FAST = /_FAST$/;
function isFast(move) {
  return FAST.test(move);
}

const LEGACY_STATUS = {
  NONE: 0,
  QUICK: 1,
  CHARGE: 2,
  BOTH: 3,
  COMMUNITY_DAY_QUICK: 5,
  COMMUNITY_DAY_CHARGE: 6,
};

function addTMCombinations(poke) {
  const comboMoves = [];

  if (!poke) return comboMoves;

  // Add all the Pokemon's current moves
  poke.moves.quick.forEach((quickMove) => {
    poke.moves.charge.forEach((chargeMove) => {
      comboMoves.push({
        A: quickMove,
        B: chargeMove,
        legacy: LEGACY_STATUS.NONE,
      });
    });
  });

  const legacy = Legacy[poke.id];
  const cdMove = communityDay[poke.id];
  if (legacy) {
    // Add legacy combos
    legacy.forEach((legacyMove) => {
      legacy.forEach((legacyMove2) => {
        if (legacyMove === legacyMove2) return;
        if (legacyMove2 === cdMove) return;

        if (isFast(legacyMove) && !isFast(legacyMove2)) {
          comboMoves.push({
            A: legacyMove,
            B: legacyMove2,
            legacy: LEGACY_STATUS.BOTH,
          });
        }
      });

      if (isFast(legacyMove)) {
        poke.moves.charge.forEach((chargeMove) => {
          comboMoves.push({
            A: legacyMove,
            B: chargeMove,
            legacy:
              legacyMove === cdMove
                ? LEGACY_STATUS.COMMUNITY_DAY_QUICK
                : LEGACY_STATUS.QUICK,
          });
        });
      } else {
        poke.moves.quick.forEach((quickMove) => {
          comboMoves.push({
            A: quickMove,
            B: legacyMove,
            legacy:
              legacyMove === cdMove
                ? LEGACY_STATUS.COMMUNITY_DAY_CHARGE
                : LEGACY_STATUS.CHARGE,
          });
        });
      }
    });
  }

  return comboMoves;
}

module.exports = addTMCombinations;
