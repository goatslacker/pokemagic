const Pokemon = require('../json/pokemon');

const scoreSort = require('./scoreSort');

const cache = {};

const ID = /^V\d/;

function findPokemon(uname) {
  if (!uname) return null;

  const num = Number(uname);
  if (!Number.isNaN(num)) {
    return Pokemon.find((poke) => poke.dex === num);
  }

  if (ID.test(uname)) {
    return Pokemon.find((poke) => poke.id === uname);
  }

  const name = uname
    .toUpperCase()
    .replace(/-/g, '_')
    .replace(/\W/g, '');
  if (cache[name]) return cache[name];

  const poke = scoreSort(Pokemon)(name, (x) => x.form || x.name);
  if (poke) cache[name] = poke;
  return poke;
}

module.exports = findPokemon;
